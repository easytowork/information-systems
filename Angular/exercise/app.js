/**
 * Created by Mims on 24.10.2014 г..
 */
(function() {
    var app = angular.module('gemStore', ['store-directives']);

    app.controller('StoreController', ['$http', function($http){
        var store = this;
        store.products = [];
        $http.get('products.json').success(function(data){
        store.products = data;
    });
    }]);

    app.controller('GalleryController', function() {
        this.imageIndex = 0;
        this.currentImageChange = function(imageNumber) {
            console.log(imageNumber);
            this.imageIndex = imageNumber || 0;
        };
    });



    app.controller("ReviewController", function() {
        this.review = {};

        this.addReview = function(product) {
            this.review.createdOn = Date.now();
            product.reviews.push(this.review);
            this.review = {};
        };
    });
})();