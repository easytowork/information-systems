﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleFactory
{
    public class NormalFormat:Names
    {
        public NormalFormat(string name)
        {
            int i = name.IndexOf(" ");
            if (i > 0)
            {
                firstName = name.Substring(0, i).Trim();
                lastName = name.Substring(i + 1).Trim();
            }
            else
            {
                lastName = name;
                firstName = "";
            }
        }
    }
}
