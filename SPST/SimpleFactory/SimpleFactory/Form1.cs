﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleFactory
{
    public partial class Form1 : Form
    {
        NameFactory nameFactory;

        public Form1()
        {
            InitializeComponent();
            nameFactory = new NameFactory();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            Names test = nameFactory.getName(txbName.Text);
            txbFirstName.Text = test.getFirstName();
            txbLastName.Text = test.getLastName();
        }
    }
}
