﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleFactory
{
    public class InverseFormat:Names
    {
        public InverseFormat(string name)
        {
            int i = name.IndexOf(", ");
            if (i > 0)
            {
                lastName = name.Substring(0, i);
                firstName = name.Substring(i + 1).Trim();
            }
            else
            {
                lastName = name;
                firstName = "";
            }
        }
    }
}
