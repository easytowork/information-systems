﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleFactory
{
    public class NameFactory
    {
        public NameFactory() { }
        public Names getName(string name)
        {
            int i = name.IndexOf(",");
            if (i > 0)
            {
                return new InverseFormat(name);
            }
            else
            {
                return new NormalFormat(name);
            }
        }
    }
}
