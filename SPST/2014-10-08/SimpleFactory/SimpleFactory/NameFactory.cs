﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SimpleFactory
{
    public class NameFactory
    {
        public NameFactory() { }
        
        /// <summary>
        ///  Factory which return Names
        /// </summary>
        /// <param name="name"></param>
        /// <returns>Names</returns>
        public Names getName(string name)
        {
            int i = name.IndexOf(",");

            if (i > 0)
            {
                return new InverseFormat(name);
            }
            else
            {
                return new NormalFormat(name);
            }
        }
    }
}
