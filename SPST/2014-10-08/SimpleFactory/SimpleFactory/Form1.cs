﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace SimpleFactory
{
    public partial class Form1 : Form
    {
        NameFactory nameFactory;

        public Form1()
        {
            InitializeComponent();
            //adding nameFactory because it will be used globally
            nameFactory = new NameFactory();
        }

        private void btnSubmit_Click(object sender, EventArgs e)
        {
            string name = txbName.Text;
            Names generatedNames = nameFactory.getName(name);

            txbFirstName.Text = generatedNames.getFirstName();
            txbLastName.Text = generatedNames.getLastName();
        }
    }
}
