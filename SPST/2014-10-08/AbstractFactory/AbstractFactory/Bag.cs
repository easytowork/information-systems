﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{
    //ProductB
    public class Bag<Brand>:IBag where Brand:IBrand, new()
    {
        private Brand myBrand;

        public Bag()
        {//we must have new, because of the constrain
            myBrand = new Brand();
        }

        public string Material
        {
            get
            {
                return myBrand.Material;
            }
        }
    }
}
