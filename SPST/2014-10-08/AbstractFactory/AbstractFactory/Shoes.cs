﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{
    //ProductA
    //c# generic constrain
    public class Shoes<Brand>:IShoes where Brand:IBrand, new()
    {
        private Brand myBrand;

        public Shoes()
        {//we must have new, because of the constrain
            myBrand = new Brand();
        }

        public int Price
        {
            get
            {
                return myBrand.Price;
            }
        }
    }
}
