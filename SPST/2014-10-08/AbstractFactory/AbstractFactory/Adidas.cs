﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{// Brand 1
    public class Adidas:IBrand
    {
        public int Price {
            get { return 400; }
        }

        public string Material {
            get { return " Crocodile leather"; }
        }
    }
}
