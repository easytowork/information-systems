﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{
    public class Client<Brand> where Brand:IBrand, new()
    {
        public void ClientMain()
        {
            IFactory<Brand> factory = new Factory<Brand>();

            IBag bag = factory.CreateBag();
            IShoes shoes = factory.CreateShoes();

            Console.WriteLine("We bought a bag which is made from " + bag.Material);
            Console.WriteLine("We bought some shoes which cost: " + shoes.Price);
        }
    }
}
