﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{
    public class Puma:IBrand
    {
        public int Price
        {
            get { return 600; }
        }

        public string Material
        {
            get { return " Carbon fiber"; }
        }
    }
}
