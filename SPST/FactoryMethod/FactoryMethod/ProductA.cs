﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryMethod
{
    class ProductA:IProduct
    {
        public String SupplyFrom()
        {
            return "from Greece";
        }
    }
}
