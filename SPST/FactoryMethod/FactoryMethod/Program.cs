﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FactoryMethod
{
    class Program  //Client
    { 
        static void Main(string[] args)
        {
            CreatorFactory c = new CreatorFactory();
            IProduct product;

            for (int i = 1; i <= 12; i++)
            {
                product = c.FactoryMethod(i);
                Console.WriteLine("Oranges "+product.SupplyFrom());
            }
        }
    }
}
