﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{   // IProductB
    public interface IBag
    {
        string Material { get; }
    }
}
