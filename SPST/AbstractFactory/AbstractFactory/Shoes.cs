﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{   //ProductA
    public class Shoes<Brand>:IShoes where Brand: IBrand, new()
    {
        private Brand myBrand;
        public Shoes()
        { //
            myBrand = new Brand();
        }
        public int Price 
        {
            get 
            {
                return myBrand.Price;
            }
        }
    }
}
