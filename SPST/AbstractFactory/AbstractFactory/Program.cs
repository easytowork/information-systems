﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{
    class Program
    {
        static void Main(string[] args)
        {
            //call client for different brand
            new Client<Adidas>().ClientMain();
            new Client<Adibas>().ClientMain();
            new Client<Puma>().ClientMain();
        }
    }
}
