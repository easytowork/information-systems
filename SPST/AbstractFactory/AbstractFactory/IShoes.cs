﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{// IProductA
    public interface IShoes
    {
        int Price { get; }
    }
}
