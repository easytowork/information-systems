﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace AbstractFactory
{ // Brand 2
    public class Adibas:IBrand
    {
        public int Price 
        {
            get { return new Adidas().Price/4; }
        }
        public string Material
        {
            get { return "Plastic"; }
        }
    }
}
