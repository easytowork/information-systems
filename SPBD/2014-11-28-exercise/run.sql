SET SERVEROUTPUT ON
/*
DECLARE
	c_id customers.id%type;
	c_name customers.name%type;
	c_addr customers.address%type;
	CURSOR c_customers is SELECT id, name, address FROM customers;
BEGIN
	OPEN c_customers;
	LOOP
		FETCH c_customers into c_id, c_name, c_addr;
		dbms_output.put_line(c_id || ' ' || c_name || ' ' || c_addr);
		EXIT WHEN c_customers%notfound;
	end loop;
	CLOSE c_customers;
	
END;
/

-- cursor based on row type 

DECLARE 
	customer_rec customers%rowtype;
BEGIN
	SELECT * into customer_rec FROM customers WHERE id=3;
	dbms_output.put_line('Customer ID: ' || customer_rec.id);
	dbms_output.put_line('Customer Name: ' || customer_rec.name);
	dbms_output.put_line('Customer Address: ' || customer_rec.address);
	dbms_output.put_line('Customer Salary: ' || customer_rec.salary);
END;
/

-- ANOTHER CURSOR
DECLARE 
	CURSOR customer_cur is SELECT id, name, address FROM customers;
	customer_rec customer_cur%rowtype;
BEGIN
	OPEN customer_cur;
	LOOP
		FETCH customer_cur into customer_rec;
		EXIT WHEN customer_cur%notfound;
		DBMS_OUTPUT.put_line(customer_rec.id || ' ' || customer_rec.name);
	END LOOP;
END;
/


DECLARE
TYPE books IS RECORD
	(
		title varchar(50),
		author varchar(50),
		subject varchar(100),
		book_id number
	);
book1 books;
book2 books;
BEGIN
	-- Book 1 specification
	book1.title := 'C programming';
	book1.author := 'Nuha Ali ';
	book1.subject := 'C programming Tutorial';
	book1.book_id := 6449407;
	
	-- Book 2 specification
	book2.title := 'Telecom Billing';
	book2.author := 'Zara Ali';
	book2.subject := 'Telecome Billing tutorial';
	book2.book_id := 6495700;
	
	-- Print book 1 record
	dbms_output.put_line('Book 1 title : ' || book1.title);
	dbms_output.put_line('Book 1 author : ' || book1.author);
	dbms_output.put_line('Book 1 subject : ' || book1.subject);
	dbms_output.put_line('Book 1 book_id : ' || book1.book_id);
	
	-- Print book 2 record
	dbms_output.put_line('Book 2 title : ' || book2.title);
	dbms_output.put_line('Book 2 author : ' || book2.author);
	dbms_output.put_line('Book 2 subject : ' || book2.subject);
	dbms_output.put_line('Book 2 book_id : ' || book2.book_id);
END;
/



DECLARE
TYPE books IS RECORD
	(
		title varchar(50),
		author varchar(50),
		subject varchar(100),
		book_id number
	);
book1 books;
book2 books;

PROCEDURE printbook(book books) IS
BEGIN
	-- Print book  record
	dbms_output.put_line('Book title : ' || book.title);
	dbms_output.put_line('Book author : ' || book.author);
	dbms_output.put_line('Book subject : ' || book.subject);
	dbms_output.put_line('Book book_id : ' || book.book_id);
END;

BEGIN
	-- Book 1 specification
	book1.title := 'C programming';
	book1.author := 'Nuha Ali ';
	book1.subject := 'C programming Tutorial';
	book1.book_id := 6449407;
	
	-- Book 2 specification
	book2.title := 'Telecom Billing';
	book2.author := 'Zara Ali';
	book2.subject := 'Telecome Billing tutorial';
	book2.book_id := 6495700;
	
	printbook(book1);
	printbook(book2);
END;
/


-- EXCEPTION EXAMPLE
DECLARE 
	c_id customers.id%type :=3;
	c_name customers.name%type;
	c_addr customers.address%type;
BEGIN
	SELECT name, address INTO c_name, c_addr FROM customers WHERE id=c_id;
	
	DBMS_OUTPUT.put_line('Name:  ' || c_name);
	DBMS_OUTPUT.put_line('Address:  ' || c_addr);
	
	EXCEPTION
		WHEN no_data_found THEN
			DBMS_OUTPUT.put_line('No such customer!');
		WHEN others THEN
			DBMS_OUTPUT.put_line('Error!');
	
END;
/

-- TRIGGERS 
CREATE OR REPLACE TRIGGER display_salary_changes
BEFORE DELETE OR INSERT OR UPDATE ON customers
FOR EACH ROW
when (NEW.ID > 0)
DECLARE sal_diff number;
BEGIN
	sal_diff := :NEW.salary - :OLD.salary;
	dbms_output.put_line('Old salary: ' || :OLD.salary);
	dbms_output.put_line('New salary : ' || :NEW.salary);
	dbms_output.put_line('Salary difference : ' || sal_diff);
	
END;
/


UPDATE customers
SET salary = salary + 500 where id = 2;

UPDATE customers
SET salary = salary + 500 where id = 3;


-- PACKAGES 

CREATE PACKAGE cust_sal AS PROCEDURE find_sal(c_id customers.id%type);
END cust_sal;
/



CREATE OR REPLACE PACKAGE BODY cust_sal AS 
PROCEDURE find_sal(c_id customers.id%type) IS c_sal customers.salary%type;
	BEGIN
		select salary INTO c_sal FROM customers WHERE id = c_id;
		dbms_output.put_line('Salary: ' || c_sal);
	END find_sal;
END cust_sal;
/


DECLARE 
	code customers.id%type := &cc_id;
BEGIN
	cust_sal.find_sal(code);
END;
/
*/


-- DISABLE AUTOCOMMIT IF WE WANT TO USE TRANSACTIONS
SET AUTOCOMMIT OFF;


INSERT INTO CUSTOMERS(ID,NAME,AGE,ADDRESS,SALARY)
VALUES(11,'Rajnish', 27,'HP',9500.00);

INSERT INTO CUSTOMERS(ID,NAME,AGE,ADDRESS,SALARY)
VALUES(12,'Riddhi', 21,'WB',4500.00);
SAVEPOINT sav1;

UPDATE cusotmers
SET SALARY = SALARY + 1000;
ROLLBACK TO sav1;

UPDATE customers
SET SALARY = SALARY + 1000
WHERE id=1;

UPDATE customers
SET SALARY = SALARY + 1000
WHERE id=2;
COMMIT;
