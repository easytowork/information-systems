
/* #1

 TABLE IS REQUIRED FURTHER IN THE EXERCISE - PLEASE IMPORT IT
CREATE TABLE CUSTOMERS(
ID INT NOT NULL,
NAME VARCHAR(20) NOT NULL,
AGE INT NOT NULL,
ADDRESS CHAR(25),
SALARY DECIMAL(18,2),
PRIMARY KEY (ID)
);
*/


/* #2 TABLE ROWS
INSERT INTO CUSTOMERS(ID,NAME,AGE,ADDRESS,SALARY)
VALUES(1,'Ramesh', 32,'Ahmedabad',2000.00);

INSERT INTO CUSTOMERS(ID,NAME,AGE,ADDRESS,SALARY)
VALUES(2,'Khilan', 25,'Delhi',1500.00);

INSERT INTO CUSTOMERS(ID,NAME,AGE,ADDRESS,SALARY)
VALUES(3,'kaushik', 23,'Kota',2000.00);

INSERT INTO CUSTOMERS(ID,NAME,AGE,ADDRESS,SALARY)
VALUES(4,'Chaitali', 25,'Mumbai',6500.00);
*/



/* #3 */
SET SERVEROUTPUT ON
DECLARE
-- Global variables
num1 number :=95;
num2 number := 85;
BEGIN
	dbms_output.put_line('Outer Variable num1: ' || num1);
	dbms_output.put_line('Outer Variable num2: ' || num2);
	DECLARE
	--Local variables
	num1 number:=195;
	num2 number :=185;
	BEGIN
	dbms_output.put_line('Inner Variable num1: ' || num1);
	dbms_output.put_line('Inner Variable num1: ' || num2);

	END;
END;
/

/* #4 */
DECLARE
	c_id customers.id%type := 1;
	c_name customers.name%type;
	c_addr customers.address%type;
	c_sal customers.salary%type;
BEGIN
	SELECT name, address, salary INTO c_name, c_addr, c_sal
	FROM customers
	WHERE id = c_id;
	dbms_output.put_line('Customer ' || c_name || ' from ' || c_addr || ' earns ' || c_sal);
END;
/

/* #5 */
DECLARE
 PI constant double precision := 3.1415;
 R number := 5;
 
 BEGIN
	dbms_output.put_line('We have PI * R * R  ' || PI * R * R);
	dbms_output.put_line('We have 2 * PI * R ' || 2* PI * R);
 END;
 /
 
 /* POWER IS **    A ** B  */
 
 /* #6 */
 DECLARE
 PROCEDURE compare(value varchar2, pattern varchar2 ) is
 BEGIN
  IF value LIKE pattern THEN
		dbms_output.put_line('True');
	ELSE
		dbms_output.put_line('False');
	END IF;
END;


BEGIN
	compare('Zara Ali ', 'Z%Ali ');
	compare('Nuha Ali','Z%Ali');
END;
/

/* #7 */
DECLARE
 x number(2) :=10;
BEGIN
	IF(x BETWEEN 5 and 20) THEN
		dbms_output.put_line('True');
	else
		dbms_output.put_line('False');
	end if;
END;
/

/* #8 */
DECLARE
	c_id customers.id%type := 1;
	c_sal customers.salary%type;
BEGIN
	SELECT salary INTO c_sal FROM customers WHERE id = c_id;
	if (c_sal <= 2000) THEN
		UPDATE customers SET salary = salary + 10000 WHERE id = c_id;
					
		SELECT salary INTO c_sal FROM customers WHERE id = c_id;
		dbms_output.put_line('Salary updated to' || c_sal );
	END IF;
END;
/

/* #9 */
DECLARE
	x number :=10;
	BEGIN
		LOOP dbms_output.put_line(x);
			x := x + 10;
			IF x > 50 THEN
				EXIT;
			end if;
		END loop;
		--AFTER EXIT, CONTROL RESUMES HERE
		DBMS_OUTPUT.PUT_LINE('After exit x is : ' || x);
END;
/

/* #10 */
DECLARE
	x number:=10;
BEGIN
	LOOP
		dbms_output.put_line(x);
		x :=x + 10;
		EXIT WHEN x > 50;
	END LOOP;
	--AFTER EXIT, CONTROL RESUMES HERE
	DBMS_OUTPUT.PUT_LINE('After exit x is : ' || x);
end;
/


/* #11 */
DECLARE 
	a number(2);
BEGIN
	FOR a in REVERSE 10..20 LOOP
		dbms_output.put_line(a);
	END LOOP;
END;
/

DECLARE 
	sumOfSalaries number :=0;
	departmentId number :=30;
BEGIN
	SELECT SUM(salary) INTO sumOfSalaries FROM employees WHERE department_id = departmentId;
	DBMS_OUTPUT.PUT_LINE('Sum of salaries : ' || sumOfSalaries  || ' for department id ' || departmentId);
END;
/

insert into customers (ID, NAME, AGE, ADDRESS, SALARY) VALUES(7,'Kriti',22,'HP', 7500.00);
			