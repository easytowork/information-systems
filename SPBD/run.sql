SET SERVEROUTPUT ON

/*
CREATE OR REPLACE
	PROCEDURE areWeWorking IS
	non_working_day EXCEPTION;
	non_working_hours EXCEPTION;
	dayOfWeek varchar(10);
	currentHour number:=0;
BEGIN
	SELECT TO_CHAR(CURRENT_DATE,'DAY') INTO dayOfWeek FROM DUAL;
	SELECT TO_CHAR(CURRENT_DATE,'HH24') INTO currentHour FROM DUAL;
	
	
	if (dayOfWeek = 'SATURDAY' OR dayOfWeek = 'SUNDAY')
		THEN  RAISE_APPLICATION_ERROR(-20205, 'We are not working during the weekend!');--RAISE non_working_day;
	else
		if (currentHour NOT BETWEEN 8 AND 18)
			THEN  RAISE_APPLICATION_ERROR(-20205, 'We are not working before 8:00 and after 18:00!');--RAISE non_working_hours;
		END IF;
	end if;
END areWeWorking;
/


CREATE OR REPLACE TRIGGER secure_employees BEFORE INSERT OR UPDATE OR DELETE ON employees
BEGIN areWeWorking;
END secure_employees;
/
*/

CREATE or REPLACE PROCEDURE add_job_history
(p_emp_id job_history.employee_id%type,
p_start_date job_history.start_date%type,
p_end_date job_history.end_date%type,
p_job_id job_history.job_id%type,
p_department_id job_history.department_id%type)
IS
BEGIN INSERT INTO job_history(employee_id, start_date,end_date,job_id,department_id)
VALUES(p_emp_id, p_start_date,p_end_date,p_job_id,p_department_id);
END add_job_history;
/


CREATE OR REPLACE TRIGGER track_employee_changes AFTER UPDATE OF job_id,department_id ON employees
FOR EACH ROW
BEGIN
	add_job_history(:old.employee_id, :old.hire_date,sysdate,:old.job_id,:old.department_id);
END track_employee_changes ;
/


