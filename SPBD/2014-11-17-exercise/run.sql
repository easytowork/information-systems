SET SERVEROUTPUT ON
DECLARE
	type namesarray IS VARRAY(5) OF VARCHAR2(10);
	type grades is VARRAY(5) OF INTEGER;
	names namesarray;
	marks grades;
	total integer;
BEGIN
	names := namesarray('Kavita', 'Pritam', 'Ayan', 'Rishav','Aziz');
	marks := grades(98,87,78,87,92);
	total := names.count;
	dbms_output.put_line('Total ' || total || ' Students');
	FOR i in 1 .. total LOOP
		dbms_output.put_line('Student: ' || names(i) || ' Marks: ' || marks(i));
	end LOOP;
	
END;
/


DECLARE
	type namesarray IS VARRAY(5) OF VARCHAR2(10);
	type grades is VARRAY(5) OF INTEGER;
	names namesarray;
	marks grades;
	total integer;
BEGIN
	names := namesarray('Kavita', 'Pritam', 'Ayan', 'Rishav','Aziz');
	marks := grades(98,87,78,87,92);
	total := names.count;
	dbms_output.put_line('Total ' || total || ' Students');
	FOR i in 1 .. total LOOP
		dbms_output.put_line('Student: ' || names(i) || ' Marks: ' || marks(i));
	end LOOP;
	
END;
/

DECLARE
	greetings varchar(30) := 'Hello world!';
BEGIN
	dbms_output.put_line(LOWER(greetings));
	dbms_output.put_line(INITCAP(greetings));
	
	/* retrieve the first character in the string */
	dbms_output.put_line(SUBSTR(greetings, 1,1));
	/* retrieve the last character in the string */
	dbms_output.put_line(SUBSTR(greetings, -1,1));
	
	/* retrive five characters starting form the seventh psition */
	dbms_output.put_line(SUBSTR(greetings, 7,5));
	
	/* retrieve the remainder of the string starting from the second position */
	dbms_output.put_line(SUBSTR(greetings, 2));
	
	/* find hte location of hte first "e" */
	dbms_output.put_line(INSTR(greetings, 'e'));
	
END;
/
			
			
DECLARE
   CURSOR c_customers is
   SELECT  name FROM customers;
   type c_list is varray (6) of customers.name%type;
   name_list c_list := c_list();
   counter integer :=0;
BEGIN
   FOR n IN c_customers LOOP
      counter := counter + 1;
      name_list.extend;
      name_list(counter)  := n.name;
      dbms_output.put_line('Customer('||counter ||'):'||name_list(counter));
   END LOOP;
   
   dbms_output.put_line('Counter ' || counter);
   counter := counter + 1;
   name_list.extend;
   name_list(counter) := 'Ninja';
   dbms_output.put_line('Customer('||counter ||'):'||name_list(counter));
   
END;
/


CREATE OR REPLACE PROCEDURE greetings
AS
BEGIN 
	dbms_output.put_line('Hello World!');
END;
/

EXECUTE greetings;
/

BEGIN
	greetings;
END;
/

DECLARE
a number;
b number;
c number;
PROCEDURE findMin(x IN number, y IN number, z OUT number) IS
BEGIN
	if x < y then 
	z:=x;
	ELSE 
	z:=y;
	END IF;
END;

BEGIN
a:=23;
b:=45;
findMin(a,b,c);
dbms_output.put_line('minimum of (23,45) : ' || c);
END;
/