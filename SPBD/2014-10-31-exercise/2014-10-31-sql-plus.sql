DECLARE 
	a integer := 10;
BEGIN
	dbms_output.put_line('Value of c: ' || 1111111111111111111111111111111/3);
END;
/

DECLARE 
	a integer := 10;
	b integer := 20;
	c integer; f real;
 BEGIN 
	 c := a + b;
	 dbms_output.put_line('Value of c: ' || c);
	 f := 70.0/3.0;
	 dbms_output.put_line('Value of f: ' || f);
 END;
/


SET SERVEROUTPUT ON
DECLARE 
	message varchar2(20):= 'Hello, World!'; 
BEGIN
 dbms_output.put_line(message);
 END;
 /
 
 DECLARE
	subtype name IS char(20);
	subtype message IS varchar2(100);
	salutation name;
	greetings message;
BEGIN
	salutation := 'Reader ';
	greetings := 'Welcome ot the World of PL/sql';
	dbms_output.put_line('Hello ' || salutation || greetings);
END;
/