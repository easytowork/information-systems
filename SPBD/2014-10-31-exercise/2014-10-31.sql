SELECT d.DEPARTMENT_NAME, COUNT(e.employee_id) FROM EMPLOYEES e 
INNER JOIN DEPARTMENTS d ON e.DEPARTMENT_ID = d.DEPARTMENT_ID GROUP BY e.DEPARTMENT_ID,d.department_name ORDER BY e.DEPARTMENT_ID 


--NAI NISKATA ZAPLATA ZA DEPERTAMENT 
SELECT MIN(SUM(salary)) min_department_salary_sum FROM employees WHERE department_id IS NOT NULL GROUP BY department_id;
-- EXPECTED RESULT: 4400

--broi employers, po departament kydeto zaplatata e po malka ot 100000
SELECT
  COUNT(employee_id),
  department_id,
  salary
FROM
  employees
GROUP BY
  department_id,
  salary
HAVING
  (
    COUNT(employee_id) > 1
  OR salary            < 100000
  )
ORDER BY
  department_id,
  salary DESC;
  
  
  --- string manipulation
  SELECT
  upper(first_name) "Last",
  initcap(first_name) "First",
  lower(email) "E-Mail"
FROM
  employees
WHERE
  department_id = 100
ORDER BY
  email;
  
  
  --date manipulation
  SELECT
  last_name,
  (extract(YEAR FROM sysdate) - extract(YEAR FROM hire_date) ) "Years Employed"
FROM
  employees
WHERE
  department_id = 100
ORDER BY
  "Years Employed";
  
  
  ---date
  SELECT
  last_name,
  hire_date, TO_CHAR(hire_date, 'FMMonth DD YYYY') "Date Started"
FROM
  employees
WHERE
  department_id = 100
ORDER BY
  last_name;
  
  
  --to number example
  SELECT
  city,
  postal_code "Old Code",
  to_number(postal_code) + 1 "New Code"
FROM
  locations
WHERE
  country_id = 'US'
ORDER BY
  postal_code;
  
  
  --deocde sample
  SELECT
  last_name,
  job_id,
  salary,
  DECODE(job_id, 'PU_CLERK', SALARY * 1.10, 
                'SH_CLERK', salary * 1.15,
                'ST_CLERK', salary * 1,20, salary) 
  "Proposed Salary"
FROM
  employees
WHERE
  job_id LIKE '%_CLERK'
AND last_name < 'E'
ORDER BY
  last_name;
  
  ---prompt for value
  SELECT
  *
FROM
  employees
WHERE
  employee_id = :employee_id;
  
  
  --temporary table
  select sysdate "NOW" from dual;
  
  --current user
  select user from dual;
  
  -- row num
  select employee_id hire_date, sysdate from employees where rownum  < 10;
  