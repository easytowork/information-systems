// QuickSort.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <iostream>
#include <iomanip>
#include<time.h>


using namespace std;
#define n 150

//int m[n]={14,6,8,1,9,4,3,7,2,11};
int m[n];

void swap(int& a, int& b){
	int temp = a;
	a=b;
	b=temp;
}

void print (int b[]){
	for(int i =0; i< n; i++)
		cout<<setw(2)<<b[i]<<" ";
	cout<< endl;

}

double diffclock(clock_t clock1,clock_t clock2)
{
	double diffticks=clock1-clock2;
	double diffms=(diffticks*1000)/CLOCKS_PER_SEC;
	return diffms;
}
 

void mas_rand_init()//inicializira masiva s random elementi avtomati4no
{
	srand((unsigned)time(NULL));
	for(int i=0;i<n;i++)
		m[i]=rand()%500;
}


void QuickSort(int b[], int l, int r)
{
	int i = l, 
		j = r; 
	
	int x = b[(i + j) / 2]; 
	do
	{
		while(b[i] < x) i++;
		while(b[j] > x) j--;
		if(i <= j)
		{
			print(m);
			cout<<" x="<<x<<" l="<<l<<",r="<<r<<" ";
			cout<<"b[i="<<i<<"]="<<b[i]<<"<->b[j="<<j<<"]="<<b[j];
			
			swap(b[i],b[j]);
			//print(m);
			//cout<<endl;
			i++;
			j--;
			cout<<" i->"<<i<<", j->"<<j<<endl;

		}
	}while(i<=j);
	if(l < j) QuickSort(b, l, j);
	if(i < r) QuickSort(b, i, r);
}
void quickSort(int b[], int left, int right) {
      int i = left, //i � �� ��������� ������ �������
	      j = right; //j � �� ��������� ������� ������ 
	  //������ 1 � ��������:
      int pivot = b[(left + right) / 2]; //����������� � ������ �������� � ������� �� ���������
      
      while (i <= j) { //������ i � j �� ��������
            while (b[i] < pivot) i++; //��������� ������,
            while (b[j] > pivot) j--; //��������� �������
            if (i <= j) { //� �������, ��� ������������ �� �� �� ��������.
				  //swap(b[i],b[j]);
				  int y= b[i];	
			      b[i]= b[j];
			      b[j]= y;


                  i++;
                  j--;
            }
      };
      //������ 2 � �������: 
      if (left < j) quickSort(b, left, j); //���������� ��������� �� ����� ��������,
      if (i < right) quickSort(b, i, right); //���������� ��������� �� ������ ��������.
	  //���������� - ������ �� ������� ������ �� �� ���� �������.
}

void QSort(int b[])
{
	quickSort(b, 0, n-1);
}

int Bin_Search(int a[], int x){
	int l=0, r=n-1;
	if((x<a[0])||(x>a[n-1])) return -1;
	while(l<=r)
	{
		int mid = (l+r)/2; //(l+r)>>1;
		if(x<a[mid]) r=mid-1;
		else
			if(a[mid]<x) l=mid+1;
			else 
			return(mid);
	}
	return -1;
}

void merge (int b[],int left, int m, int right)
{
//��������� merge ����� ��� ��������� 
//��������� b[left...m] � b[m+1....right] � ���� c[]
	right = right +1;
	m = m+1;
	int l=left;
	int c[n]; //������� �����
	int p=m, q=m, rl = right-left;
	int i = 0;
	while (i<rl && left!=p && q!=right) //������ �� � ��������� ����� �� ������ ��������:
	{
		if(b[left]<b[q])
		{
			c[i]=b[left++]; //����� �� ��-������ �� ����� ���������
		}
		else{
			c[i]=b[q++]; //�������� � �������� � ������ c[].
		}
		i++;
	}
	if(left == p){
		while(i < rl){
			c[i++]=b[q++];
		}
	}
	if(q == right){
		while (i < rl){
			c[i++]=b[left++];
		}
	}
//������������ ������ b[] �� ���������� c[],
//�.�. �� �� ����� ���� �������� 
	for(i=0; i < rl; i++)
		b[l++]=c[i];

}

void MergeSort(int b[],int left, int right){
	int m;
	if(left < right)//�������� �� ���� �� ���������� � �������� �� ���� ������� (���� ��� ����� �� �� �������)
	{
		m = (right+left)/2;//������ �� ����������� �� ��� ���������
		MergeSort(b,left, m); //��������� �� ����� ���
		MergeSort(b,m+1,right); //��������� �� ������ ���
		merge(b,left,m,right); // ������� �� ����� ����
	}
}
void MSort(int b[])
{
	MergeSort(b,0,n-1);
}

void _tmain(void)
{
	mas_rand_init();
	clock_t begin,end;
	cout<<"Masiw predi sortirane:"<<endl<<endl;
	//print(m);
	cout<< endl<<"Masiv sled sortirane: "<< endl<<endl;
	//sort_m(m);
	begin = clock();
	QSort(m);
	//MSort(m);
	
	end = clock();
	print(m);
	cout << "Time elapsed: " << double(diffclock(end,begin)) << " ms"<< endl;
	cout<<Bin_Search(m,8)<<endl;
		
}
