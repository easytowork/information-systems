﻿// MatrixChainMultiplication.cpp : Defines the entry point for the console application.
//



#include <stdio.h>

#define MAX 100
#define INFINITY (unsigned long)(-1)
#define NOT_SOLVED (unsigned long)(-1)

unsigned long m[MAX][MAX];  //Lookup Table 

//Dimensions of the matrices

// Matrix Ai has dimension r[i-1] x r[i] for i = 1..MAX

//const unsigned long r[MAX+1] = {12,13,35,3,34,2,21,10,21,6};
//const unsigned n = 9; /* Number matrices */


//Example:
// A = [10x30]
// B = [30x5]
// C = [5x60]

//(AB)C = (10×30×5) + (10×5×60) = 1500 + 3000 = 4500 operations
//A(BC) = (30×5×60) + (10×30×60) = 9000 + 18000 = 27000 operations.
const unsigned long r[MAX+1] = {10, 30, 5, 60};
const unsigned n = 3; // Number matrices 
// End Example;


// Nonefficient recursive implementation 
unsigned long solveRecursive(unsigned i, unsigned j)
{ 
	unsigned k;
	if (i == j) return 0;
	m[i][j] = INFINITY;
	for (k = i; k <= j - 1; k++) {
		unsigned long q = solveRecursive(i, k) + 	solveRecursive(k + 1, j) +  	r[i - 1] * r[k] * r[j];
		if (q < m[i][j])
			m[i][j] = q;
	}
	return m[i][j];
}


void printMatrix(void) // Print matrix of the minimums 
{
	unsigned i,j;
	printf("\nMatrix of the all min:");
	for (i = 1; i <= n; i++) {
		printf("\n");

		for (j = 1; j <= n; j++)
			printf("%8lu", m[i][j]);
	}
}

//======================================
long solveMemo(unsigned i, unsigned j)
{ 
	unsigned k;
	unsigned long q;
	if (NOT_SOLVED != m[i][j]) // Check for already computed
		return m[i][j];

	if (i == j) // In this interval there is no matrix
		m[i][j] = 0;
	else { //Calculate recursively
		for (k = i; k <= j - 1; k++)
			if ((q = solveMemo(i,k)+solveMemo(k+1,j)+r[i-1]*r[k]*r[j]) < m[i][j])
				m[i][j] = q;
	}
	return m[i][j];
}


long solveMemoization(void)
{ 
	unsigned i, j;
	for (i = 1; i <= n; i++)
		for (j = i; j <= n; j++)
			m[i][j] = NOT_SOLVED;
	return solveMemo(1, n);
}
//=======================================

int main(void) {
printf("\nMin number of multiplication: %lu",solveRecursive(1,n));
//printf("\nMin number of multiplication: %lu",solveMemoization());
printMatrix();
printf("\n");
return 0;
}