// Combinations.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
using namespace std;

int *r, n, k;

void combin(int m)
{
	if(m <= k)
	{
		for(r[m] = r[m-1] + 1; r[m] <= n-k+m; r[m]++)
			combin(m+1);
	}
	else
	{
		for(int i = 1; i <= k; i++)
			printf("%d ", r[i]);
		printf("\n");
	}
}

void main()
{
	do{
		printf("Enter n and k (k not greater than n): ");
		cin>>n>>k;
	} while(n < 0 || k < 0 || k > n);
	r= new int[k + 1];
	r[0] = 0;

	printf("\nCombinations:\n");
	combin(1);

}

