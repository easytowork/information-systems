// CombinationStepByStep.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
using namespace std;

int *r, n, k;

int nextComb()
{
	//��� ���� �� �� �������� ����� ���������, �� ��
	//��������� � r[1], ..., r[k] � �� ����� �������� 1.
	//� �������� ������ ��������� �������� � 0.
	int i = k, j;
	//�������� ���-�������� i, �� ����� r[i]
	//���� �� ���� ���������:
	while (i > 0 && r[i] == n - k + i)
		i--;
	if(i==0) return 0; //���� ���� ����������
	//����������� r[i] � ������ ������ �� r[i+1], ..., r[k]
	//������� ���-����� �������� ���������
	//(���������� �� r �� � ��������� ���).
	r[i]++;
	for(j = i + 1; j <= k; j++)
		r[j] = r[j-1] + 1;
	return 1;

}

void main()
{
	printf("Iterative combination generator for k elements");
	printf(" out of 1, 2, ..., n.\n");

	do{
		printf("Enter n and k (k not greater than n): ");
		cin>>n>>k;
	} while(n < 0 || k < 0 || k > n);
	r= new int[k + 1];

	for(int i=1; i<=k; i++)
		r[i]=i;

	//==========
	int code, i, j=0;
	do
	{
		printf("\nEnter 1 to display the next combination or 0 to stop:");
		//scanf("%d",code);
		cin>>code;
		if(code == 0) return;
		printf("Combination %d: ",++j);
		for(i=1; i<=k; i++)
			printf("%d ",r[i]);
		printf("\n\n");
	} while(nextComb());
} 
