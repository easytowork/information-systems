// Permutations.cpp : Defines the entry point for the console application.
//

//#include "stdafx.h"
#include <iostream>
using namespace std;

int *r, n;

void permut(int k)
/*
	����������� permut(k) ��������� ����� �� (n-k+1)! ����. 
	��� ����� �� ��� ��� n ���� ����� r[1],...,r[k-1], r[k],...,r[n], 
	���� r[1],...,r[k-1]
	�� �� �������, � r[k],...,r[n] ��������� ������ ���������� �� 
	���� ���-����� n-k+1 ��������.
	������������ ����������� permut(1) ��������� �������� n! �� 
	���� ���������� �� r[1],...,r[n].
	������������ �� ���������� � ��������� ���.
*/
{
	int i, j, tmp;
	if(k <= n)
		for(i=k; i<=n; i++)
		{
			/*
				�� ����� i ��������� r[i] �� ��������� �� ������� k, 
				� ������� �������� r[k],...,r[i-1] �� ���������� 
				� ���� ������� �������
			*/
			tmp = r[i];
			for(j=i;j>k;j--) r[j]= r[j-1];
			r[k] = tmp;
			// ��� �� ��������� r[k], ��������� ���������� ��������� 
			//��� r[k+1],...,r[n]:
			permut(k+1);
			// �������������� ���������� ��������:
			for(j=k; j<i; j++) r[j] = r[j+1];
			r[i] = tmp;
		}
	else
	{
		//��� k==n, ������� �� ������ ���� �� ���� ����������, 
		//����� ���� �����������:
		for(i=1; i<=n; i++)
			printf("%d  ",r[i]);
		printf("\n");
	}

}

void main()
{
	printf("Enter n:");
	//scanf("%d",&n);
	cin>>n;

	r=new int[n+1];
	for(int i=1; i<=n;i++)
		r[i]=i;

	printf("Permutations:\n");
	permut(1);
	delete []r;
}

