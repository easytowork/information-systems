// nested_for.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include<iostream>
using namespace std;

const unsigned  int N = 3;
int n;
int lower[] = {5,2,8};
int upper[] = {7,2,9};
int r[N];
int *low;
int *upp;
int *rr;

void PrintArray(int a[], int n)
{
	int i;
	for(i=0; i<n; i++)
	{
		printf("%d%  ",a[i]);
	}
	printf("\n");
}

void f(int k)
{
	if(k==n) PrintArray(rr,n);
	else
		for(rr[k]=low[k]; rr[k]<=upp[k]; rr[k]++)
			f(k+1);
}

void fIterative()
{
	int i , n1 = n -1;
	for(i=0; i<n; i++)
	{
		if(low[i]> upp[i]) return;
		else rr[i] = low[i];
	}
	for(;;)
	{
		PrintArray(rr,n);
		i=n1;
		for(;;)
		{
			if(rr[i] < upp[i]){rr[i]++ ; break;}
			rr[i] = low[i];
			if(--i < 0) return;
		}
	}
}

void uint2bin(unsigned int num, int level)
{
	unsigned int i, k;
	k=1<<level-1;
	i= 1<<(sizeof(num)*8 -1);
	while(i>0)
	{
		if(i<=k)
		{
			if(num&i)
				printf("1");
			else
				printf(".");
		}
		i>>=1;

	}
}

inline unsigned int gray_code(unsigned int x){return x^(x>>1);}

void main(int argc, _TCHAR* argv[])
{ /*
	printf("Enter n:");
	cin>>n;
	low = new int[n];
	upp = new int[n];
	rr= new int[n];

	printf("Enter %d pairs of (low, upp):\n",n);
	for(int i = 0; i<n; i++)
		cin>>low[i]>>upp[i];
	printf("\n\n");
	f(0);
	printf("\n============\n");
	fIterative();
	printf("\n\n");
	*/

	for(int i = 0; i<=31; i++)
	{
		printf("%2d%:  ",i);
		uint2bin(i,7);
		printf("   ");
		uint2bin(gray_code(i),7);
		printf("\n");
	}
}

