//Solves the problem for the knight's walk			Version: 21.11.2012
//on a chess-board of dimension n x n.				File: Knight_Walk.cpp
//-----------------------------------------
#include "stdafx.h"
#include <iostream>
#include <iomanip>

using namespace std;

const short maxn= 15;	//maximal dimension of the board
const short r[]={-2, -2, -1, -1,  1, 1,  2, 2},
			c[]={-1,  1, -2,  2, -2, 2, -1, 1};
short n, sqrn, solnum=0;	//n is the real dimension of the board
short board[maxn][maxn];	

void print_solution (short &m)	//prints the solution number m
{	m++;
	cout<<"Solution number "<<m<<"\n";
	for (short i=0; i<n; i++)
	{	for (int j=0; j<n; j++)
			cout<<setw(3)<<board[i][j];
		cout<<'\n';
	}
	cout<<"\n";
}

//after the i-th move the knight is in position [x,y] on the board
void try_first (short i, short x, short y, bool &is_found)	//tries to make the (i+1)-st move,
{	short j= 0, u, v;										//searches only the first solution
	while ((j<8) && !is_found)
	{	u= x+r[j], v= y+c[j];
		bool inside= u>=0 && u<n && v>=0 && v<n;
		if (inside && (board[u][v]==0))
		{	board[u][v]= i+1;
			if (i+1<sqrn) try_first (i+1, u, v, is_found);
			else 
			{	print_solution (solnum);
				is_found= true;
			}
			board[u][v]= 0;
		}
		j++;
	}
}
//after the i-th move the knight is in position [x,y] on the board
void try_all (short i, short x, short y)		//tries to make the (i+1)-st move 
{	for (int j= 0; j<8; j++)					//searches all solutions
	{	short u= x+r[j], v= y+c[j];
		bool inside= u>=0 && u<n && v>=0 && v<n;
		if (inside && (board[u][v]==0))
		{	board[u][v]= i+1;
			if (i+1<sqrn)	try_all (i+1, u, v);
			else	print_solution (solnum);
			board[u][v]= 0;
		}
	}
}

void main ()
{	do
	{	cout<<"The dimension of the chess-board is: ";
		cin>> n;
	} 
	while (n<3 || n>maxn);
	sqrn= n*n;
	cout<<"Enter the initial position of the knight:\n";
	short ir, ic;
	do
	{	cout<<"The row number [1,"<<n<<"] is: ";
		cin>> ir;
	} 
	while (ir<1 || ir>n);
	do
	{	cout<<"The column number [1,"<<n<<"] is: ";
		cin>> ic;
	} 
	while (ic<1 || ic>n);
	for (short i= 0; i<n; i++)	
		for (short j= 0; j<n; j++)	
			board[i][j]= 0;
	ir--;	ic--;
	board[ir][ic]= 1;
	bool found= false;
//	try_first (1, ir, ic, found);
	try_all (1, ir, ic);
	if (solnum==0) cout<<"No solution!\n";
	system ("pause");
}
